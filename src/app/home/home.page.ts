import { Component } from '@angular/core';
import { ConfigurationRestService } from '../data/configuration-rest.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
    site_name:string;
    site_description:string;

    /**
     * Constructor:
     *  - Register service
     */
    constructor(private configurationSercice: ConfigurationRestService) {
      console.log('RestConnectorService : constructor');
      configurationSercice.get('site_name').subscribe(
        (data:any) => {
          this.site_name=data.value;
        },
        (error) => {
          console.log("Erreur RestConnectorService");
          console.log(error);
        }
      );

      configurationSercice.get('site_description').subscribe(
        (data:any) => {
          this.site_description=data.value;
        },
        (error) => {
          console.log("Erreur RestConnectorService");
          console.log(error);
        }
      );
    }

}
