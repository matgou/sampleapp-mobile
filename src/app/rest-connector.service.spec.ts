import { TestBed } from '@angular/core/testing';

import { RestConnectorService } from './rest-connector.service';

describe('RestConnectorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RestConnectorService = TestBed.get(RestConnectorService);
    expect(service).toBeTruthy();
  });
});
