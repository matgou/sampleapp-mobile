import { TestBed } from '@angular/core/testing';

import { ConfigurationRestService } from './configuration-rest.service';

describe('ConfigurationRestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ConfigurationRestService = TestBed.get(ConfigurationRestService);
    expect(service).toBeTruthy();
  });
});
