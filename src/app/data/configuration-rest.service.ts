import { Injectable } from '@angular/core';
import { RestConnectorService } from '../rest-connector.service';
import { Http } from '@angular/http';
import { GuidService } from '../utils/guid.service';
import { AuthenticationService } from '../api/authentication.service';
import { Observable } from 'rxjs';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationRestService extends RestConnectorService {

  constructor(http: Http, guidProvider: GuidService, auth: AuthenticationService, nav:NavController) {
    super(http, guidProvider, auth, nav);
    this.objectType = 'configuration';

    console.log('ConfigurationRestService : constructor');
  }

  public getAll(): Observable<{}> {
    return this._find(this.objectType, '');
  }

    public get(objectId): Observable<{}> {
      return this._getByFilter(this.objectType, 'name=eq.' + objectId);
    }

}
