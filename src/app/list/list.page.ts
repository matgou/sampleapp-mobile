import { Component, OnInit } from '@angular/core';
import { NoteRestService } from '../data/note-rest.service';

@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})
export class ListPage implements OnInit {

  public items: any;
  constructor(private noteService:NoteRestService) {  }

  ngOnInit() {
    this.noteService.getAll().subscribe(
      (data) => {
        this.items = data;
      }
    )
  }
}
