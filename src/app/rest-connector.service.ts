import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { GuidService } from './utils/guid.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthenticationService } from './api/authentication.service';
import { NavController } from '@ionic/angular';

/**
 * RestConnectorService
 * Abstract class to manipulate data from PostgRest
 *
 * Author: Mathieu GOULIN <mathieu.goulin@gadz.org>
 */
export abstract class RestConnectorService {
  /**
   * URL of webservice
   */
  private BASE_URL="http://localhost:8300";

  /**
   * Store de objectType ie table name
   */
  protected objectType:string;

  /**
   * Constructor:
   *  - Register service
   */
  constructor(private httpClient: Http,
              private UUIDGenerator: GuidService,
              private auth: AuthenticationService,
              private navCtrl: NavController) {
    console.log('RestConnectorService : constructor');
    console.log(navCtrl);
  }

  /**
   * Construct RequestOptions with Authorization Bearer
   */
  private getOptions(authorizationMandatory?:boolean) {
    if(authorizationMandatory === undefined) {
      authorizationMandatory=true;
    }
    let headers = new Headers({'Content-Type': 'application/json'});
    if(this.auth !== null) {
      if(this.auth.isAuthenticated()) {
        headers.append('Authorization','Bearer ' + this.auth.getToken());
      }
    } else if(authorizationMandatory == true) {
      if(this.navCtrl !== null) {
        this.navCtrl.navigateRoot("LoginPage");
      }
    }
    return new RequestOptions({headers: headers});
  }

  /**
   * Construct URL from objectType and filter
   */
  private buildUrl(objectType:string, filter:string) {
      return this.BASE_URL + '/' + objectType + '?' + filter;
  }

  /**
   * Update object
   */
  public _save(objectType:string, updatedObject:any): Observable<{}> {
    console.log("Send request to save " + objectType);
    return this.httpClient.patch(this.buildUrl(objectType,'id=eq.' + updatedObject.id), updatedObject, this.getOptions()).pipe(
      catchError(this.handleError)
    );
  }

  /**
   * Insert object
   */
  public _push(objectType:string, object:any): Observable<{}> {
    console.log("Send post request to " + objectType);
    if(object.id == undefined) {
      let guid = this.UUIDGenerator.newGuid();
      console.log("generate guid: " + guid)
      object.id = guid;
    }

    return this.httpClient.post(this.buildUrl(objectType,''), object, this.getOptions()).pipe(
      catchError(this.handleError)
    );
  }


  /**
   * Call a procedure
   */
  public _rpc(functionName:string) {
    return this.httpClient.get(this.BASE_URL + '/rpc/' + functionName, this.getOptions()).pipe(map(res => res.json()));
  }

  public __rpc(functionName:string, object:any) {
    return this.httpClient.post(this.BASE_URL + '/rpc/' + functionName, object, this.getOptions()).pipe(map(res => res.json()));
  }

  /**
   * Select multiple object
   */
  public _find(objectType:string, filter:string) {
    console.log("Get object (" + objectType + ") where : " + filter);
    return this.httpClient.get(this.buildUrl(objectType,filter), this.getOptions()).pipe(
      map(res => res.json()),
      catchError(this.handleError)
    );
  }

  /**
   * Select unique object
   */
  public _getByFilter(objectType:string, filter:string) {
    let object = Observable.create((observer) => {
      // Call
      let response = this.httpClient.get(this.buildUrl(objectType, filter), this.getOptions()).pipe(map(res => res.json()));
      response.subscribe(
        (data) => {
          if(data.length == 1) {
            observer.next(data[0]);
          } else {
            observer.error("Unable to find uniq object whith key : " + filter);
          }
        },
        (error) => { observer.error(error); }
      );
    });
    return object;
  }

  public _get(objectType:string, objectId:string) {
    return this._getByFilter(objectType, 'id=eq.' + objectId);
  }

  protected handleError(error: HttpErrorResponse) {
    console.log(error);
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    let object = Observable.create((observer) => {
      observer.error('Une erreur s\'est produite, réessayez plus tard.');
    });
    return object;
  };
}
