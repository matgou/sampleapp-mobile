import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoteEditPage } from './note-edit.page';

describe('NoteEditPage', () => {
  let component: NoteEditPage;
  let fixture: ComponentFixture<NoteEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoteEditPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoteEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
