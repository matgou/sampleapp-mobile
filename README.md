# SampleApp mobile

An simple application to start a mobile application project. Based on Ionic 4 framework.

## Usage

Start application développement with docker-compose and ionic

    docker-compose up -d
    ionic serve --lab -c

## Architecture

![Architecture](https://framagit.org/matgou/sampleapp-mobile/raw/master/architecture.png?inline=false)

## Add new table or object

### PostgreSQL Table

Modify sql/schema.sql file to add your table and initial-data

### The service
Create a new service with this command :

    ionic g service data/<table_name>-rest

And upgrade file src/app/data/<table_name>-rest.service.ts

    import { Injectable } from '@angular/core';
    import { RestConnectorService } from '../rest-connector.service';
    import { Http } from '@angular/http';
    import { GuidService } from '../utils/guid.service';
    import { AuthenticationService } from '../api/authentication.service';
    import { Observable } from 'rxjs';
    import { NavController } from '@ionic/angular';

    @Injectable({
      providedIn: 'root'
    })
    export class NoteRestService extends RestConnectorService {

      constructor(http: Http, guidProvider: GuidService, auth: AuthenticationService, nav:NavController) {
        super(http, guidProvider, auth, nav);
        this.objectType = 'note';

        console.log('ConfigurationRestService : constructor');
      }

      public getAll(): Observable<{}> {
        return this._find(this.objectType, '');
      }

      public get(objectId): Observable<{}> {
        return this._get(this.objectType,objectId);
      }

      public save(updatedObject): Observable<{}> {
        return this._save(this.objectType, updatedObject);
      }

      public push(object): Observable<{}> {
        return this._push(this.objectType, object);
      }
    }

## Add new Page

Create a new page with this command :

    ionic g page <page_name>
